import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReactivoComponent } from './components/reactivo/reactivo.component';

const routes: Routes = [
  { path:'formularioreclamo', component: ReactivoComponent},
  { path:'**', pathMatch:'full', redirectTo:'formularioreclamo'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
