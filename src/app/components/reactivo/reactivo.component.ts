import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-reactivo',
  templateUrl: './reactivo.component.html',
  styleUrls: ['./reactivo.component.css']
})
export class ReactivoComponent implements OnInit {
  forma!: FormGroup;
  constructor(private fb: FormBuilder,
              private validores: Validators) {
    this.crearFormulario();
   }

  ngOnInit(): void {
  }
get nombreNoValido(){
  return this.forma.get('nombre')?.invalid && this.forma.get('nombre')?.touched;
}

get apellidoNoValido(){
  return (this.forma.get('apellido')?.invalid && !this.forma.get('apellido')?.errors?.['noZaralai']) && this.forma.get('apellido')?.touched;
}
  
crearFormulario():void{
  this.forma = this.fb.group({
    nombre: ['',[Validators.required, Validators.minLength(4)]],
    apellido: ['', [Validators.required, Validators.minLength(4), Validators.pattern(/^[a-zA-ZñÑ\s]+$/)]],
  })
}

guardar():void{
  console.log(this.forma.value);
  //Reset del formulario
  this.LimpiarFormulario();
}

LimpiarFormulario(){
  //this.forma.reset();
  this.forma.reset({
    nombre: 'Pedro'
  });
}

}
